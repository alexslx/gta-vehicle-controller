/*
 * Copyright 2015 Alexandre Leites. All rights reserved.
 *
 * Contributors:
 * - Alexandre Leites	[Head Developer]
 * - Brett Mayson		[Cruise Control]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include <cstdio>
#include <memory>
#include <cstdarg>
#include <initializer_list>

#include "script.h"

/*
 * Function Prototypes
 */
void ChangeDoorsState(Vehicle veh, std::initializer_list<int> doors, int *door_state, bool noOpenAnimation, bool noCloseAnimation);
void SetIndicatorLights(Vehicle veh, bool leftIndicatorState, bool rightIndicatorState);

/*
 * Global Variables
 */
int keyDefPressDelay		= 500;

int keyDefLeftLight			= 74;
int keyDefLeftLightAlt		= -1;

int keyDefRightLight		= 76;
int keyDefRightLightAlt		= -1;

int keyDefAlertLight		= 75;
int keyDefAlertLightAlt		= -1;

int keyDefHoodSwitch		= 74;
int keyDefHoodSwitchAlt		= 17;

int keyDefTrunkSwitch		= 76;
int keyDefTrunkSwitchAlt	= 17;

int keyDefInteriorSwitch	= 75;
int keyDefInteriorSwitchAlt	= 17;

int keyDefExtraSwitch		= 73;
int keyDefExtraSwitchAlt	= 0;

int keyDefEngineSwitch		= 79;
int keyDefEngineSwitchAlt	= 0;

int keyDefLeaveVehicle		= 71;
int keyDefLeaveVehicleAlt	= 17;
float maxSpeedLeaveVehicle	= 11.17;

int keyDefCruiseControl		= 84;
int keyDefCruiseControlAlt	= 0;

int keyDefWindowControl		= 106;
int keyDefWindowControlAlt	= 17;

int speedometerFontType		= 0;
int speedometerFontColorR	= 255;
int speedometerFontColorG	= 255;
int speedometerFontColorB	= 255;
int speedometerFontColorA	= 255;
float speedometerPosTop		= 500.0;
float speedometerPosLeft	= 0;
float speedometerFontScale	= 0.25;

DWORD keyPressDelay = 0;

BOOL firstCheck				= FALSE;
BOOL leftLight				= FALSE;
BOOL rightLight				= FALSE;
BOOL hoodOpen				= FALSE;
BOOL trunkOpen				= FALSE;
BOOL trailerOpen			= FALSE;
BOOL interiorLight			= FALSE;
BOOL extraSwitch			= FALSE;
BOOL engineSwitch			= FALSE;
BOOL windowsOpen			= FALSE;
BOOL speedometer			= FALSE;
BOOL speedometerInKmh		= FALSE;
BOOL breakLightsState		= FALSE;
BOOL breakLightsWhenStopped = FALSE;
BOOL breakLightsUntilMove	= FALSE;

Vehicle skyLiftAttachedVeh	= NULL;

float cruiseSpeed	= 0.00f;
float lastSpeed		= 0.00f;

std::string statusText;
DWORD statusTextDrawTicksMax;
bool statusTextGxtEntry;

bool CheckForActiveWindow()
{
	DWORD pid;
	GetWindowThreadProcessId(GetForegroundWindow(), &pid);

	return (GetCurrentProcessId() == pid);
}


void UpdateDebugText()
{
#ifdef _DEBUG
	if (GetTickCount() < statusTextDrawTicksMax)
	{
		UI::SET_TEXT_FONT(0);
		UI::SET_TEXT_SCALE(0.55, 0.55);
		UI::SET_TEXT_COLOUR(255, 255, 255, 255);
		UI::SET_TEXT_WRAP(0.0, 1.0);
		UI::SET_TEXT_CENTRE(1);
		UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
		UI::SET_TEXT_EDGE(1, 0, 0, 0, 205);
		if (statusTextGxtEntry)
		{
			UI::_SET_TEXT_ENTRY((char *)statusText.c_str());
		} else
		{
			UI::_SET_TEXT_ENTRY("STRING");
			UI::_ADD_TEXT_COMPONENT_STRING((char *)statusText.c_str());
		}
		UI::_DRAW_TEXT(0.5, 0.5);
	}
#endif
}

void SetDebugText(std::string str, DWORD time = 2500, bool isGxtEntry = false)
{
#ifdef _DEBUG
	statusText = str;
	statusTextDrawTicksMax = GetTickCount() + time;
	statusTextGxtEntry = isGxtEntry;
#endif
}

void draw_text(char* text, int font_type, int font_color[], float lineSize[], float linePosition[], float font_scale, float textLeft)
{
	int screen_w, screen_h;
	float lineWidthScaled, lineHeightScaled;
	float lineTopScaled, lineLeftScaled;
	float textLeftScaled;

	GRAPHICS::GET_SCREEN_RESOLUTION(&screen_w, &screen_h);

	// Size
	lineWidthScaled    = lineSize[0] / (float)screen_w;					// line width
	lineHeightScaled   = lineSize[1] / (float)screen_h;					// line height

	// Position
	lineTopScaled      = linePosition[0] / (float)screen_h;				// top offset
	lineLeftScaled     = linePosition[1] / (float)screen_w;				// left offset

	/*
	* Negative values will be the offset from right or bottom.
	*/
	/*if (linePosition[0] < 0)
		lineTopScaled  = (float)screen_h - linePosition[0];

	if (linePosition[1] < 0)
		lineLeftScaled = (float)screen_w - linePosition[1];*/

	// Text position
	textLeftScaled     = (linePosition[1] + textLeft) / (float)screen_w;	// left offset

	// Text upper part
	UI::SET_TEXT_FONT(font_type);
	UI::SET_TEXT_SCALE(0.0, font_scale);
	UI::SET_TEXT_COLOUR(font_color[0], font_color[1], font_color[2], font_color[3]);
	UI::SET_TEXT_CENTRE(0);
	UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
	UI::SET_TEXT_EDGE(0, 0, 0, 0, 0);
	UI::_SET_TEXT_ENTRY("STRING");
	UI::_ADD_TEXT_COMPONENT_STRING(text);
	UI::_DRAW_TEXT(textLeftScaled, (((lineTopScaled + 0.00278f) + lineHeightScaled) - 0.005f));

	// Text lower part
	UI::SET_TEXT_FONT(font_type);
	UI::SET_TEXT_SCALE(0.0, font_scale);
	UI::SET_TEXT_COLOUR(font_color[0], font_color[1], font_color[2], font_color[3]);
	UI::SET_TEXT_CENTRE(0);
	UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
	UI::SET_TEXT_EDGE(0, 0, 0, 0, 0);
	UI::_SET_TEXT_GXT_ENTRY("STRING");
	UI::_ADD_TEXT_COMPONENT_STRING(text);

	/*
	 * NOTICE: I don't know why this line is important, but without it, other NotifyAboveMap() stop working...
	 * PS: This work without the line, but breaks the NotifyAboveMap()... weird.
	 */
	int num25 = UI::_0x9040DFB09BE75706(textLeftScaled, (((lineTopScaled + 0.00278f) + lineHeightScaled) - 0.005f));
}

void DisplayHelpMessage(char *message, int time)
{
	UI::_0x8509B634FBE7DA11((Any *)message);
	UI::_0x238FFE5C7B0498A6(0, false, true, time);
}

void NotifyAboveMap(char* message)
{
	UI::_SET_NOTIFICATION_TEXT_ENTRY("STRING");
	UI::_ADD_TEXT_COMPONENT_STRING(message);
	UI::_DRAW_NOTIFICATION(FALSE, TRUE);
}

void draw_speedometer(char* value)
{
	float lineSize[2], linePosition[2], font_scale, textLeft;
	int font_color[4], font_type;

	lineSize[0]     = 350.0;
	lineSize[1]     = 15.0;
	linePosition[0] = speedometerPosTop;
	linePosition[1] = speedometerPosLeft;

	textLeft        = 0.0;
	font_scale      = speedometerFontScale;

	font_color[0]   = MAX(MIN(speedometerFontColorR, 255), 0);
	font_color[1]   = MAX(MIN(speedometerFontColorG, 255), 0);
	font_color[2]   = MAX(MIN(speedometerFontColorB, 255), 0);
	font_color[3]   = MAX(MIN(speedometerFontColorA, 255), 0);

	font_type       = speedometerFontType;

	draw_text(value, font_type, font_color, lineSize, linePosition, font_scale, textLeft);
}

std::string string_format(const std::string fmt_str, ...)
{
	int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
	std::string str;
	std::unique_ptr<char[]> formatted;
	va_list ap;
	while (1) {
		formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
		strcpy(&formatted[0], fmt_str.c_str());
		va_start(ap, fmt_str);
		final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
		va_end(ap);
		if (final_n < 0 || final_n >= n)
			n += abs(final_n - n + 1);
		else
			break;
	}
	return std::string(formatted.get());
}

Vector3 operator +(const Vector3& v1, const Vector3& v2)
{
	Vector3 ret;
	ret.x = v1.x + v2.x;
	ret.y = v1.y + v2.y;
	ret.z = v1.z + v2.z;
	ret._paddingx = v1._paddingx + v2._paddingx;
	ret._paddingy = v1._paddingy + v2._paddingy;
	ret._paddingz = v1._paddingz + v2._paddingz;
	return ret;
}

Vector3 operator *(const Vector3& v1, const int& v2)
{
	Vector3 ret;
	ret.x = v1.x * v2;
	ret.y = v1.y * v2;
	ret.z = v1.z * v2;
	ret._paddingx = v1._paddingx * v2;
	ret._paddingy = v1._paddingy * v2;
	ret._paddingz = v1._paddingz * v2;
	return ret;
}

Vector3 operator -(const Vector3& v1, const Vector3& v2)
{
	return v1 + (v2 * -1);
}

void reset()
{
	firstCheck			= FALSE;
	leftLight			= FALSE;
	rightLight			= FALSE;
	hoodOpen			= FALSE;
	trunkOpen			= FALSE;
	trailerOpen			= FALSE;
	interiorLight		= FALSE;
	extraSwitch			= FALSE;
	engineSwitch		= TRUE;
	cruiseSpeed			= 0.00f;
	breakLightsState	= FALSE;
	windowsOpen			= FALSE;

	if (skyLiftAttachedVeh)
	{
		ENTITY::DETACH_ENTITY(skyLiftAttachedVeh, 1, 1);
		skyLiftAttachedVeh = NULL;
	}
}

void readConfigFile()
{
	INIReader reader("VehicleController.ini");
	if (reader.ParseError() < 0)
		return;

	keyDefPressDelay		= reader.GetInteger("Keyboard", "keyDefPressDelay", keyDefPressDelay);

	keyDefLeftLight			= reader.GetInteger("Keyboard", "keyDefLeftLight", keyDefLeftLight);
	keyDefLeftLightAlt		= reader.GetInteger("Keyboard", "keyDefLeftLightAlt", keyDefLeftLightAlt);

	keyDefRightLight		= reader.GetInteger("Keyboard", "keyDefRightLight", keyDefRightLight);
	keyDefRightLightAlt		= reader.GetInteger("Keyboard", "keyDefRightLightAlt", keyDefRightLightAlt);

	keyDefAlertLight		= reader.GetInteger("Keyboard", "keyDefAlertLight", keyDefAlertLight);
	keyDefAlertLightAlt		= reader.GetInteger("Keyboard", "keyDefAlertLightAlt", keyDefAlertLightAlt);

	keyDefHoodSwitch		= reader.GetInteger("Keyboard", "keyDefHoodSwitch", keyDefHoodSwitch);
	keyDefHoodSwitchAlt		= reader.GetInteger("Keyboard", "keyDefHoodSwitchAlt", keyDefHoodSwitchAlt);

	keyDefTrunkSwitch		= reader.GetInteger("Keyboard", "keyDefTrunkSwitch", keyDefTrunkSwitch);
	keyDefTrunkSwitchAlt	= reader.GetInteger("Keyboard", "keyDefTrunkSwitchAlt", keyDefTrunkSwitchAlt);

	keyDefInteriorSwitch	= reader.GetInteger("Keyboard", "keyDefInteriorSwitch", keyDefInteriorSwitch);
	keyDefInteriorSwitchAlt	= reader.GetInteger("Keyboard", "keyDefInteriorSwitchAlt", keyDefInteriorSwitchAlt);

	keyDefEngineSwitch		= reader.GetInteger("Keyboard", "keyDefEngineSwitch", keyDefEngineSwitch);
	keyDefEngineSwitchAlt	= reader.GetInteger("Keyboard", "keyDefEngineSwitchAlt", keyDefEngineSwitchAlt);

	keyDefExtraSwitch		= reader.GetInteger("Keyboard", "keyDefExtraSwitch", keyDefExtraSwitch);
	keyDefExtraSwitchAlt	= reader.GetInteger("Keyboard", "keyDefExtraSwitchAlt", keyDefExtraSwitchAlt);
	
	keyDefLeaveVehicle		= reader.GetInteger("Keyboard", "keyDefLeaveVehicle", keyDefLeaveVehicle);
	keyDefLeaveVehicleAlt	= reader.GetInteger("Keyboard", "keyDefLeaveVehicleAlt", keyDefLeaveVehicleAlt);

	keyDefCruiseControl		= reader.GetInteger("Keyboard", "keyDefCruiseControl", keyDefCruiseControl);
	keyDefCruiseControlAlt	= reader.GetInteger("Keyboard", "keyDefCruiseControlAlt", keyDefCruiseControlAlt);

	keyDefWindowControl		= reader.GetInteger("Keyboard", "keyDefWindowControl", keyDefWindowControl);
	keyDefWindowControlAlt	= reader.GetInteger("Keyboard", "keyDefWindowControlAlt", keyDefWindowControlAlt);

	speedometer				= reader.GetBoolean("Speedometer", "Enabled", speedometer);
	speedometerInKmh		= reader.GetBoolean("Speedometer", "ShowSpeedInKmh", speedometerInKmh);
	speedometerFontColorR	= reader.GetInteger("Speedometer", "FontColorRed", speedometerFontColorR);
	speedometerFontColorG	= reader.GetInteger("Speedometer", "FontColorGreen", speedometerFontColorG);
	speedometerFontColorB	= reader.GetInteger("Speedometer", "FontColorBlue", speedometerFontColorB);
	speedometerFontColorA	= reader.GetInteger("Speedometer", "FontColorAlpha", speedometerFontColorA);
	speedometerFontType		= reader.GetInteger("Speedometer", "FontType", speedometerFontType);
	speedometerFontScale	= reader.GetReal("Speedometer", "FontScale", speedometerFontScale);
	speedometerPosTop		= reader.GetReal("Speedometer", "OffsetFromTop", speedometerPosTop);
	speedometerPosLeft		= reader.GetReal("Speedometer", "OffsetFromLeft", speedometerPosLeft);

	maxSpeedLeaveVehicle	= reader.GetInteger("General", "MaxSpeedLeaveVehicle", maxSpeedLeaveVehicle);
	maxSpeedLeaveVehicle	*= speedometerInKmh ? 0.277777778 : 0.44704;

	breakLightsWhenStopped	= reader.GetBoolean("General", "BreakLightsWhenStopped", breakLightsWhenStopped);
	breakLightsUntilMove	= reader.GetBoolean("General", "breakLightsUntilMove", breakLightsUntilMove);
}

void setKeyPressDelay()
{
	keyPressDelay = GetTickCount() + keyDefPressDelay;
}

bool isKeyDown(int key, int alt)
{
	if (alt == -1 && !NOALTKEYS() || alt > 0 && !KEYDOWN(alt))
		return false;

	return KEYDOWN(key);
}

void main()
{
	readConfigFile();
	reset();

	while (true)
	{
		WAIT(0);
		UpdateDebugText();

		Ped playerPed		= PLAYER::PLAYER_PED_ID();
		Vehicle playerVeh	= PED::GET_VEHICLE_PED_IS_USING(playerPed);

		/*
		 * Check if the player and his/her vehicle are correctly registered into game engine, as well as if the player is alive.
		 * In case of any error, player not being in a vehicle or player being dead, reset the variables.
		 */
		if (!ENTITY::DOES_ENTITY_EXIST(playerPed) || !PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0) || PLAYER::IS_PLAYER_DEAD(playerPed))
		{
			reset();
			continue;
		}

		/*
		 * Check if the player is the vehicle driver or not (to avoid online misusage).
		 */
		BOOL isPlayerDriver			= (VEHICLE::GET_PED_IN_VEHICLE_SEAT(playerVeh, -1) == playerPed);
		Vector3 playerVehPos		= ENTITY::GET_ENTITY_COORDS(playerVeh, 1);
		unsigned int playerVehModel	= ENTITY::GET_ENTITY_MODEL(playerVeh);
		float playerVehSpeed		= ENTITY::GET_ENTITY_SPEED(playerVeh);

		bool isGTAActive			= CheckForActiveWindow();

		Vehicle attachedVehicle		= NULL;
		VEHICLE::GET_VEHICLE_TRAILER_VEHICLE(playerVeh, &attachedVehicle);

		if (!attachedVehicle)
			trailerOpen				= FALSE;

		if (isPlayerDriver)
		{
			/*
			 * Display the vehicle speed for the driver.
			 */
			if (speedometer)
			{
				char speedBuffer[10];
				float speed = (float)(playerVehSpeed * 2.25); // meters per second to miles per hour

				/*
				 * 1 mile per hour:
				 * - kph: mph * 1.60934
				 * - knot: mph * 0.868976242
				 */

				if (speedometerInKmh)
					sprintf(speedBuffer, "%.f Km/h", round(speed * 1.60934));
				else
					sprintf(speedBuffer, "%.f MPH", round(speed));


				draw_speedometer(speedBuffer);
			}

			/*
			 * Kill vehicle engine.
			 * Rockstar's lazy developers didn't check if you're entering the car or just sitting there.
			 * That's why we need to kill the engine every cycle, if not, the player will turn it on again.
			 */
			if (!engineSwitch)
				VEHICLE::SET_VEHICLE_ENGINE_ON(playerVeh, FALSE, FALSE);

			/*
			 * Cruising control.
			 */
			if (cruiseSpeed != 0.00f)
			{
				if (lastSpeed == 0.00f)
					lastSpeed = playerVehSpeed;

				if (isGTAActive && ( CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_ACCELERATE) ||  CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_BRAKE) || CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_MOVE_LR) ) )
				{
					// Player is controlling the vehicle, lay down for a second.
				}
				else if (isGTAActive && CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_HANDBRAKE))
				{
					cruiseSpeed	= 0.00f;
					lastSpeed	= 0.00f;
					NotifyAboveMap((char *)string_format("Cruise Control - <C>~%s~%s</C>", TOGGLECOLOUR(FALSE), TOGGLESTRING(FALSE)).c_str());
				}
				else if ((abs(lastSpeed) - abs(playerVehSpeed)) >= MIN(10, abs(lastSpeed * 0.2)))
				{
					cruiseSpeed = 0.00f;
					lastSpeed	= 0.00f;
					NotifyAboveMap((char *)string_format("Cruise Control - <C>~%s~%s</C>", TOGGLECOLOUR(FALSE), TOGGLESTRING(FALSE)).c_str());
				}
				else if ( (cruiseSpeed != playerVehSpeed) && (ENTITY::GET_ENTITY_HEIGHT_ABOVE_GROUND(playerVeh) < 1))
				{
					int increment		= cruiseSpeed - playerVehSpeed;
					increment			= MAX(MIN(increment, 1), -1);
					lastSpeed			= playerVehSpeed;
					breakLightsState	= FALSE;
					VEHICLE::SET_VEHICLE_FORWARD_SPEED(playerVeh, playerVehSpeed + increment);
				}
			}

			if (isGTAActive)
			{
				/*
				 * Driver is breaking
				 */
				if (CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_BRAKE) && breakLightsUntilMove)
					breakLightsState = TRUE;

				/*
				 * Driver is accelerating again
				 */
				if (CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_ACCELERATE) && breakLightsUntilMove)
					breakLightsState = FALSE;
			}

			/*
			 * Break Lights
			 */
			if (breakLightsState || breakLightsWhenStopped && playerVehSpeed == 0.00f)
			{
				VEHICLE::SET_VEHICLE_BRAKE_LIGHTS(playerVeh, true);

				if (attachedVehicle)
					VEHICLE::SET_VEHICLE_BRAKE_LIGHTS(attachedVehicle, true);
			}
		}

		/*
		 * Check for keypressing minimal delay to avoid press duplication.
		 */
		if (GetTickCount() < keyPressDelay || !isGTAActive)
			continue;

		if (isPlayerDriver)
		{
			/*
			* Vehicle turning indicators controller.
			*/
			if (isKeyDown(keyDefAlertLight, keyDefAlertLightAlt))
			{
				setKeyPressDelay();

				if (leftLight != rightLight)
					leftLight = rightLight = TRUE;
				else if (leftLight == TRUE)
					leftLight = rightLight = FALSE;
				else
					leftLight = rightLight = TRUE;

				SetIndicatorLights(playerVeh, leftLight, rightLight);

				if (attachedVehicle)
					SetIndicatorLights(attachedVehicle, leftLight, rightLight);
			}
			else if (isKeyDown(keyDefLeftLight, keyDefLeftLightAlt) || isKeyDown(keyDefRightLight, keyDefRightLightAlt))
			{
				setKeyPressDelay();
				rightLight	= isKeyDown(keyDefRightLight, keyDefRightLightAlt) ? !rightLight : FALSE;
				leftLight	= isKeyDown(keyDefLeftLight, keyDefLeftLightAlt)   ? !leftLight  : FALSE;

				SetIndicatorLights(playerVeh, leftLight, rightLight);

				if (attachedVehicle)
					SetIndicatorLights(attachedVehicle, leftLight, rightLight);
			}


			/*
			* Vehicle hood controller.
			*/
			if (isKeyDown(keyDefHoodSwitch, keyDefHoodSwitchAlt))
			{
				setKeyPressDelay();
				hoodOpen = !hoodOpen;

				ChangeDoorsState(playerVeh, { 4 }, &hoodOpen, FALSE, FALSE);
			}


			/*
			* Vehicle trunk controller.
			*/
			if (isKeyDown(keyDefTrunkSwitch, keyDefTrunkSwitchAlt))
			{
				setKeyPressDelay();
				trunkOpen	= !trunkOpen;

				if (IS_CARGOBOB(playerVehModel) || playerVehModel == VEHICLE_CARGOPLANE)
				{
					if (trunkOpen)
						VEHICLE::SET_VEHICLE_DOOR_OPEN(playerVeh, 2, FALSE, TRUE);
					else
						VEHICLE::SET_VEHICLE_FIXED(playerVeh); // VEHICLE::SET_VEHICLE_DOOR_SHUT() doesn't work properly with these models.
				}
				else if (playerVehModel == VEHICLE_MULE || playerVehModel == VEHICLE_MULE2 || playerVehModel == VEHICLE_MULE3 || playerVehModel == VEHICLE_POUNDER)
				{
					ChangeDoorsState(playerVeh, {2, 3}, &trunkOpen, FALSE, FALSE);
				}
				else
				{
					BOOL skipAnimation = !VEHICLE::IS_THIS_MODEL_A_CAR(playerVehModel);
					ChangeDoorsState(playerVeh, { 5 }, &trunkOpen, skipAnimation, skipAnimation);

					/*
					* Open attached vehicle doors.
					*/
					if (attachedVehicle)
					{
						unsigned int attachedModel = ENTITY::GET_ENTITY_MODEL(attachedVehicle);
						trailerOpen = !trailerOpen;

						if (attachedModel == VEHICLE_TR2 || attachedModel == VEHICLE_TR4)
							ChangeDoorsState(attachedVehicle, { 5 }, &trailerOpen, FALSE, TRUE);
						else
							ChangeDoorsState(attachedVehicle, { 1, 2, 3, 4, 5 }, &trailerOpen, FALSE, FALSE);
					}
				}
			}


			/*
			* Vehicle interior lights controller.
			*/
			if (isKeyDown(keyDefInteriorSwitch, keyDefInteriorSwitchAlt))
			{
				setKeyPressDelay();
				interiorLight = !interiorLight;
				VEHICLE::SET_VEHICLE_INTERIORLIGHT(playerVeh, interiorLight);

				NotifyAboveMap((char *)string_format("Interior Light - <C>~%s~%s</C>", TOGGLECOLOUR(interiorLight), TOGGLESTRING(interiorLight)).c_str());
			}


			/*
			* Vehicle engine controller.
			*/
			if (isKeyDown(keyDefEngineSwitch, keyDefEngineSwitchAlt))
			{
				setKeyPressDelay();
				engineSwitch = !engineSwitch;

				// The player AI will turn on the engines alone in cars, so I don't need to call it here.
				if (!engineSwitch || !VEHICLE::IS_THIS_MODEL_A_CAR(playerVehModel) || !VEHICLE::IS_THIS_MODEL_A_QUADBIKE(playerVehModel))
					VEHICLE::SET_VEHICLE_ENGINE_ON(playerVeh, engineSwitch, engineSwitch);

				NotifyAboveMap((char *)string_format("Engine - <C>~%s~%s</C>", TOGGLECOLOUR(engineSwitch), TOGGLESTRING(engineSwitch)).c_str());
			}

			/*
			 * Skylift Magnet Controller
			 */
			if (playerVehModel == VEHICLE_SKYLIFT && CONTROLS::IS_CONTROL_JUST_PRESSED(0, INPUT_VEH_GRAPPLING_HOOK))
			{
				Vector3 vehRotation = ENTITY::GET_ENTITY_ROTATION(playerVeh, 2);

				if (skyLiftAttachedVeh)
				{
					ENTITY::DETACH_ENTITY(skyLiftAttachedVeh, 1, 1);

					skyLiftAttachedVeh = NULL;
					NotifyAboveMap("Skylift - Magnet <C>~HUD_COLOUR_RED~OFF</C>");
				}
				else
				{
					Vehicle targetVeh	= VEHICLE::GET_CLOSEST_VEHICLE(playerVehPos.x, playerVehPos.y, playerVehPos.z, 10.0f, 0, 70);
					if (targetVeh != NULL)
					{
						Vector3 min, max;
						GAMEPLAY::GET_MODEL_DIMENSIONS(ENTITY::GET_ENTITY_MODEL(targetVeh), &min, &max);

						ENTITY::ATTACH_ENTITY_TO_ENTITY(targetVeh, playerVeh, 0, 0, -(max.y)+(max.y/4), -max.z, 0, 0, 0, 1, 0, 1, 0, 2, 1);

						skyLiftAttachedVeh = targetVeh;
						NotifyAboveMap("Skylift - Magnet <C>~HUD_COLOUR_BLUE~ON</C>");
					}
				}
			}


			/*
			* Vehicle extra's feature controller.
			* For taxis, it will control the taxi passenger indicator.
			* For police helicopter, it will switch the spotlight.
			*/
			if (isKeyDown(keyDefExtraSwitch, keyDefExtraSwitchAlt))
			{
				// Taxi
				if (playerVehModel == VEHICLE_TAXI)
				{
					setKeyPressDelay();
					extraSwitch = !extraSwitch;

					VEHICLE::SET_TAXI_LIGHTS(playerVeh, extraSwitch);
					NotifyAboveMap((char *)string_format("Taxi Light - <C>~%s~%s</C>", TOGGLECOLOUR(extraSwitch), TOGGLESTRING(extraSwitch)).c_str());
				}
				else if (VEHICLE::IS_THIS_MODEL_A_HELI(playerVehModel) && playerVehModel != VEHICLE_SKYLIFT && !IS_CARGOBOB(playerVehModel))
				{
					setKeyPressDelay();
					extraSwitch = !extraSwitch;

					VEHICLE::SET_VEHICLE_SEARCHLIGHT(playerVeh, extraSwitch, extraSwitch);
					NotifyAboveMap((char *)string_format("Search Light - <C>~%s~%s</C>", TOGGLECOLOUR(extraSwitch), TOGGLESTRING(extraSwitch)).c_str());
				}
			}


			/*
			 * Vehicle cruise control.
			 * Press the button while speeding to set your cruise speed.
			 * Press it again to reset the cruise speed.
			 */
			if (isKeyDown(keyDefCruiseControl, keyDefCruiseControlAlt))
			{
				setKeyPressDelay();
				if (cruiseSpeed == 0.00f)
				{
					cruiseSpeed = playerVehSpeed;
					lastSpeed	= playerVehSpeed;
					NotifyAboveMap((char *)string_format("Cruise Control - <C>~%s~%s</C>", TOGGLECOLOUR(TRUE), TOGGLESTRING(TRUE)).c_str());
				}
				else
				{
					cruiseSpeed = 0.00f;
					lastSpeed	= 0.00f;
					NotifyAboveMap((char *)string_format("Cruise Control - <C>~%s~%s</C>", TOGGLECOLOUR(FALSE), TOGGLESTRING(FALSE)).c_str());
				}
			}
		}

		/*
		 * Vehicle window feature
		 */
		if (isKeyDown(keyDefWindowControl, 0))
		{
			setKeyPressDelay();
			windowsOpen = !windowsOpen;

			for (int i = 0; i < 4; i++)
			{
				if (VEHICLE::GET_PED_IN_VEHICLE_SEAT(playerVeh, i-1) == playerPed || isPlayerDriver && isKeyDown(keyDefWindowControl, keyDefWindowControlAlt))
				{
					if (windowsOpen)
						VEHICLE::ROLL_DOWN_WINDOW(playerVeh, i);
					else
						VEHICLE::ROLL_UP_WINDOW(playerVeh, i);
				}
			}
		}

		/*
		 * Vehicle leaving features.
		 */
		if (isKeyDown(keyDefLeaveVehicle, 0))
		{
			if (PED::IS_PED_IN_ANY_HELI(playerPed) && ENTITY::GET_ENTITY_HEIGHT_ABOVE_GROUND(playerVeh) < 40.0f)
			{
				for (int i = 2; i < 4; i++)
				{
					if (VEHICLE::GET_PED_IN_VEHICLE_SEAT(playerVeh, i - 1) == playerPed)
					{
						AI::CLEAR_PED_TASKS(playerPed);
						AI::TASK_RAPPEL_FROM_HELI(playerPed, 0x41200000);
						break;
					}
				}
			}
			else if (playerVehSpeed <= maxSpeedLeaveVehicle)
			{
				int flags = 0;
				/*
				* If the player is just a passenger, it will let the door open without the need of
				* pressing the alternate key, if not, the player will need to hold it to let the door open.
				*/
				if (!isPlayerDriver || isKeyDown(keyDefLeaveVehicle, keyDefLeaveVehicleAlt))
					flags = 1 << 8;

				AI::CLEAR_PED_TASKS(playerPed);
				AI::TASK_LEAVE_VEHICLE(playerPed, playerVeh, flags);

				/*
				* If the player is the vehicle driver, let the engine running.
				*/
				if (isPlayerDriver)
				{
					WAIT(5);
					VEHICLE::SET_VEHICLE_ENGINE_ON(playerVeh, TRUE, TRUE);
				}
			}
		}
	}
}

void ChangeDoorsState(Vehicle veh, std::initializer_list<int> doors, int *door_state, bool noOpenAnimation, bool noCloseAnimation)
{
	for (int i : doors)
	{
		if (*door_state)
		{
			VEHICLE::SET_VEHICLE_DOOR_OPEN(veh, i, TRUE, noOpenAnimation);
		}
		else
		{
			VEHICLE::SET_VEHICLE_DOOR_SHUT(veh, i, noCloseAnimation);
		}
	}
}

void SetIndicatorLights(Vehicle veh, bool leftIndicatorState, bool rightIndicatorState)
{
	VEHICLE::SET_VEHICLE_INDICATOR_LIGHTS(veh, TRUE, leftIndicatorState);
	VEHICLE::SET_VEHICLE_INDICATOR_LIGHTS(veh, FALSE, rightIndicatorState);
}

void ScriptMain()
{
	main();
}
