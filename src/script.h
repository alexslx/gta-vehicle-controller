/*
 * Copyright 2015 Alexandre Leites. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#pragma warning(disable : 4244 4305 4800) // double <-> float conversions

#include <stdarg.h>  // For va_start, etc.
#include <memory>    // For std::unique_ptr

#include "..\ScriptHook.h"

#include "..\3rdparty\inih\ini.h"
#include "..\3rdparty\inih\INIReader.h"

#include "game\inputs.h"
#include "game\vehicles.h"
#include "game\weapons.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define TOGGLESTRING(x) ((x) == TRUE) ? "ON":"OFF"
#define TOGGLECOLOUR(x) ((x) == TRUE) ? "HUD_COLOUR_BLUE":"HUD_COLOUR_RED"

#define KEYDOWN(key) (GetAsyncKeyState(key) & 0x8000 ? 1 : 0)

#define NOCTRLKEY() (!KEYDOWN(VK_CONTROL) && !KEYDOWN(VK_RCONTROL) && !KEYDOWN(VK_LCONTROL))
#define NOSHIFTKEY() (!KEYDOWN(VK_SHIFT) && !KEYDOWN(VK_RSHIFT) && !KEYDOWN(VK_LSHIFT))
#define NOMENUKEY() (!KEYDOWN(VK_MENU) && !KEYDOWN(VK_RMENU) && !KEYDOWN(VK_LMENU))

#define NOALTKEYS() (NOCTRLKEY() && NOSHIFTKEY() && NOMENUKEY())

void ScriptMain();
